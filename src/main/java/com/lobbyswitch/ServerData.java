package com.lobbyswitch;

import com.lobbyswitch.ping.ServerListPing;
import com.lobbyswitch.ping.StatusResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.scheduler.BukkitRunnable;

public class ServerData implements PluginMessageListener {
    private ExecutorService executorService = null;
    private String name;
    private String ip;
    private short port;
    private String MOTD = "Offline";
    private int playerCount;
    private int maxPlayers;
    private String version;

    public ServerData(String name) {
        this.name = name;
        (new BukkitRunnable() {
            public void run() {
                ServerData.this.updateData();
                ServerData.this.updateInventories();
            }
        }).runTaskTimer(LobbySwitch.p, 20L, (long)LobbySwitch.p.getConfig().getInt("MOTD_Refresh_Rate"));
        this.executorService = Executors.newFixedThreadPool(1);
    }

    public void updateData() {
        if (!LobbySwitch.p.getServer().getOnlinePlayers().isEmpty()) {
            Player player = (Player)LobbySwitch.p.getServer().getOnlinePlayers().toArray()[0];
            if (LobbySwitch.p.getServers().keySet().contains(this.name)) {
                if (LobbySwitch.p.getServer().getName().equals(this.name)) {
                    this.ip = LobbySwitch.p.getServer().getIp();
                    this.port = (short)LobbySwitch.p.getServer().getPort();
                    this.playerCount = LobbySwitch.p.getServer().getOnlinePlayers().size();
                } else {
                    var bytes = new ByteArrayOutputStream();
                    var dataOutput = new DataOutputStream(bytes);
                    try {
                        dataOutput.writeUTF("ServerIP");
                        dataOutput.writeUTF(this.name);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }
                    player.sendPluginMessage(LobbySwitch.p, LobbySwitch.p.getPluginChannel(), bytes.toByteArray());
                    this.getServerListPing(new Callback<StatusResponse>() {
                        public void onSuccess(StatusResponse response) {
                            ServerData.this.playerCount = response.getPlayers().getOnline();
                            ServerData.this.maxPlayers = response.getPlayers().getMax();
                            ServerData.this.MOTD = response.getDescription().replace("Â", "");
                            ServerData.this.version = response.getVersion().getName();
                        }

                        public void onFailure() {
                            ServerData.this.playerCount = 0;
                            ServerData.this.maxPlayers = 0;
                            ServerData.this.MOTD = "OFFLINE";
                        }
                    });
                }
            }
        }

    }

    public void updateInventories() {
        for (var player : Bukkit.getOnlinePlayers()) {
            var view = player.getOpenInventory();
            if (view.getTitle().equals(LobbySwitch.p.getConfig().getString("Inventory.Name"))) {
                view.getTopInventory().setContents(LobbySwitch.p.getConfigManager().getInventory().getContents());
            }
        }

    }

    public String getIp() {
        return this.ip;
    }

    public String getMOTD() {
        return this.MOTD;
    }

    public short getPort() {
        return this.port;
    }

    public String getName() {
        return this.name;
    }

    public int getPlayerCount() {
        return this.playerCount;
    }

    private void getServerListPing(final Callback<StatusResponse> callback) {
        this.executorService.submit(new Runnable() {
            public void run() {
                ServerListPing serverListPing = new ServerListPing();
                serverListPing.setAddress(new InetSocketAddress(ServerData.this.ip, ServerData.this.port));

                try {
                    callback.onSuccess(serverListPing.fetchData());
                } catch (IOException var3) {
                    callback.onFailure();
                }

            }
        });
    }

    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (channel.equals(LobbySwitch.p.getPluginChannel())) {
            try {
                var dataInput = new DataInputStream(new ByteArrayInputStream(message));
                String subChannel = dataInput.readUTF();
                if (subChannel.equals("ServerIP") && dataInput.readUTF().equals(this.name)) {
                    this.ip = dataInput.readUTF();
                    this.port = dataInput.readShort();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
