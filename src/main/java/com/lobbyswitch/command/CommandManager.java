package com.lobbyswitch.command;

import com.lobbyswitch.LobbySwitch;
import com.lobbyswitch.command.commands.AddCommand;
import com.lobbyswitch.command.commands.DefaultCommand;
import com.lobbyswitch.command.commands.EditCommand;
import com.lobbyswitch.command.commands.ListCommand;
import com.lobbyswitch.command.commands.OpenCommand;
import com.lobbyswitch.command.commands.RemoveCommand;
import com.lobbyswitch.command.commands.VersionCommand;
import org.bukkit.command.PluginCommand;

public class CommandManager {
    private final CommandDispatcher commandDispatcher;

    public CommandManager() {
        PluginCommand pluginCommand = LobbySwitch.p.getCommand("lobbyswitch");
        this.commandDispatcher = new CommandDispatcher("lobbyswitch", "LobbySwitch's main command.");
        this.commandDispatcher.setDefault(new DefaultCommand());
        this.commandDispatcher.registerCommand(new AddCommand());
        this.commandDispatcher.registerCommand(new DefaultCommand());
        this.commandDispatcher.registerCommand(new EditCommand());
        this.commandDispatcher.registerCommand(new ListCommand());
        this.commandDispatcher.registerCommand(new OpenCommand());
        this.commandDispatcher.registerCommand(new RemoveCommand());
        this.commandDispatcher.registerCommand(new VersionCommand());
        pluginCommand.setExecutor(this.commandDispatcher);
        pluginCommand.setTabCompleter(this.commandDispatcher);
    }

    public CommandDispatcher getCommandDispatcher() {
        return this.commandDispatcher;
    }
}
