package com.lobbyswitch.ping;

import java.util.List;

public class Players {
    private int max;
    private int online;
    private List<Player> sample;

    public int getMax() {
        return this.max;
    }

    public int getOnline() {
        return this.online;
    }

    public List<Player> getSample() {
        return this.sample;
    }
}
