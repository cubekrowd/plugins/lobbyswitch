package com.lobbyswitch.ping.impl;

import com.lobbyswitch.ping.StatusResponse;

public class StatusResponse1_9 extends StatusResponse {
    private Description description;

    public String getDescription() {
        return this.description.getText();
    }

    public class Description {
        private String text;

        public String getText() {
            return this.text;
        }
    }
}
