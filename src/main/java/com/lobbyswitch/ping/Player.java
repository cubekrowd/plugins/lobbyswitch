package com.lobbyswitch.ping;

public class Player {
    private String name;
    private String id;

    public String getName() {
        return this.name;
    }

    public String getId() {
        return this.id;
    }
}
