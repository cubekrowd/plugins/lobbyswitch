package com.lobbyswitch;

import com.lobbyswitch.command.CommandManager;
import com.lobbyswitch.config.ConfigManager;
import com.lobbyswitch.config.ConfigUpdater;
import com.lobbyswitch.listeners.CypherInventoryListener;
import com.lobbyswitch.listeners.CypherPlayerListener;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class LobbySwitch extends JavaPlugin {
    public static LobbySwitch p;
    private FileConfiguration config;
    private FileConfiguration messages;
    private ConfigManager configManager;
    private CommandManager commandManager;
    private HashMap<String, ServerData> servers = new HashMap<>();
    private String pluginChannel = "BungeeCord";

    public void onEnable() {
        p = this;
        loadConfig();
        configManager = new ConfigManager(config);
        commandManager = new CommandManager();
        registerListener(Bukkit.getPluginManager());
        if (getServer().getPluginManager().getPlugin("RedisBungee") != null) {
            pluginChannel = "RedisBungee";
        }

        loadMessages();
        Bukkit.getMessenger().registerOutgoingPluginChannel(this, pluginChannel);
        Bukkit.getMessenger().registerIncomingPluginChannel(this, pluginChannel, new CypherPlayerListener());
        if (!Bukkit.getOnlinePlayers().isEmpty()) {
            p.getServer().getScheduler().scheduleSyncDelayedTask(p, () -> {
                var bytes = new ByteArrayOutputStream();
                var dataOutput = new DataOutputStream(bytes);
                try {
                    dataOutput.writeUTF("GetServers");
                    Bukkit.getOnlinePlayers().iterator().next().sendPluginMessage(LobbySwitch.p, "BungeeCord", bytes.toByteArray());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }, 20L);
        }

    }

    public FileConfiguration getMessages() {
        return messages;
    }

    public FileConfiguration getFileConfig() {
        return config;
    }

    public ConfigManager getConfigManager() {
        return configManager;
    }

    public CommandManager getCommandManager() {
        return commandManager;
    }

    public HashMap<String, ServerData> getServers() {
        return servers;
    }

    public void addServer(String server) {
        if (!servers.containsKey(server)) {
            ServerData serverData = new ServerData(server);
            Bukkit.getMessenger().registerIncomingPluginChannel(this, pluginChannel, serverData);
            servers.put(server, serverData);
        }

    }

    private void registerListener(PluginManager pluginManager) {
        HandlerList.unregisterAll(this);
        CypherInventoryListener inventoryListener = new CypherInventoryListener();
        CypherPlayerListener playerListener = new CypherPlayerListener();
        pluginManager.registerEvents(inventoryListener, this);
        pluginManager.registerEvents(playerListener, this);
    }

    private void loadConfig() {
        (new ConfigUpdater(getConfig())).update();
        getConfig().options().copyDefaults(true);
        saveConfig();
        config = getConfig();
    }

    private void loadMessages() {
        try (var resource = getTextResource("messages.yml")) {
            messages = YamlConfiguration.loadConfiguration(resource);
        } catch (IOException e) {
            getLogger().log(Level.SEVERE, "Cannot load internal messages.yml", e);
        }
    }

    public String getPluginChannel() {
        return pluginChannel;
    }
}
