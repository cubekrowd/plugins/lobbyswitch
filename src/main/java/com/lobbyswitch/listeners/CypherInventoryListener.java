package com.lobbyswitch.listeners;

import com.lobbyswitch.LobbySwitch;
import com.lobbyswitch.ServerItem;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

public class CypherInventoryListener implements Listener {
    @EventHandler
    public void onInventoryClickEvent(InventoryClickEvent event) {
        // cancel all interactions when the LobbySwitch inventory is open, so
        // players can't pick up items to drag click, can't shift click items
        // around, etc. etc.
        if (event.getView().getTitle().replace("§", "&").equals(LobbySwitch.p.getConfig().getString("Inventory.Name"))) {
            event.setCancelled(true);
        }

        var clickedInventory = event.getInventory();

        // handle clicking on items in the LobbySwitch inventory
        if (clickedInventory == event.getWhoClicked().getOpenInventory().getTopInventory()
                && event.getView().getTitle().replace("§", "&").equals(LobbySwitch.p.getConfig().getString("Inventory.Name"))
                && clickedInventory.getSize() >= event.getSlot() && event.getSlot() >= 0) {
            ItemStack itemStack = clickedInventory.getItem(event.getSlot());
            if (itemStack != null && event.getWhoClicked() instanceof Player) {
                Iterator var3 = LobbySwitch.p.getConfigManager().getSlots().iterator();

                while(var3.hasNext()) {
                    String string = (String)var3.next();
                    if (event.getSlot() == Integer.parseInt(string) - 1) {
                        Player player = (Player)event.getWhoClicked();
                        ServerItem serverItem = LobbySwitch.p.getConfigManager().getServerItem(Integer.parseInt(string));
                        var bytes = new ByteArrayOutputStream();
                        var dataOutput = new DataOutputStream(bytes);
                        try {
                            dataOutput.writeUTF("Connect");
                            dataOutput.writeUTF(serverItem.getTargetServer());
                            player.sendPluginMessage(LobbySwitch.p, "BungeeCord", bytes.toByteArray());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        if (LobbySwitch.p.getConfigManager().getFileConfiguration().getBoolean("Selector.Slot.Forced")) {
            InventoryView inventoryView = event.getView();
            ItemStack selector = LobbySwitch.p.getConfigManager().getSelector();
            ItemStack affectedStack = inventoryView.getItem(event.getRawSlot());
            if (affectedStack != null && affectedStack.equals(selector)) {
                event.setCancelled(true);
            } else if (event.getAction() == InventoryAction.HOTBAR_SWAP || event.getAction() == InventoryAction.HOTBAR_MOVE_AND_READD) {
                affectedStack = inventoryView.getItem(event.getHotbarButton());
                if (affectedStack != null && affectedStack.equals(selector)) {
                    event.setCancelled(true);
                } else {
                    affectedStack = inventoryView.getBottomInventory().getItem(event.getHotbarButton());
                    if (affectedStack != null && affectedStack.equals(selector)) {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }
}
