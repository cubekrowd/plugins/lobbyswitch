package com.lobbyswitch.command;

import com.lobbyswitch.LobbySwitch;
import com.lobbyswitch.util.ChatUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.bukkit.ChatColor;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.RemoteConsoleCommandSender;
import org.bukkit.command.TabCompleter;

public class CommandDispatcher implements CommandExecutor, TabCompleter {
    private final String commandName;
    private final String commandDescription;
    private final Map<String, ICommand> subCommands;
    private ICommand command;

    public CommandDispatcher(String commandName, String description) {
        this.commandName = commandName;
        this.commandDescription = description;
        this.subCommands = new LinkedHashMap();
    }

    public void registerCommand(ICommand command) {
        this.subCommands.put(command.getName(), command);
    }

    public void setDefault(ICommand command) {
        this.command = command;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0 && this.command == null) {
            this.displayUsage(sender, label, (String)null);
            return true;
        } else {
            ICommand com = null;
            String subCommand = "";
            String[] subArgs = args;
            if (args.length > 0) {
                subCommand = args[0].toLowerCase();
                subArgs = args.length > 1 ? (String[])Arrays.copyOfRange(args, 1, args.length) : new String[0];
                if (this.subCommands.containsKey(subCommand)) {
                    com = (ICommand)this.subCommands.get(subCommand);
                } else {
                    Iterator var8 = this.subCommands.entrySet().iterator();

                    label95:
                    while(true) {
                        Entry ent;
                        do {
                            if (!var8.hasNext()) {
                                break label95;
                            }

                            ent = (Entry)var8.next();
                        } while(((ICommand)ent.getValue()).getAliases() == null);

                        String[] aliases = ((ICommand)ent.getValue()).getAliases();
                        String[] var11 = aliases;
                        int var12 = aliases.length;

                        for(int var13 = 0; var13 < var12; ++var13) {
                            String alias = var11[var13];
                            if (subCommand.equalsIgnoreCase(alias)) {
                                com = (ICommand)ent.getValue();
                                break label95;
                            }
                        }
                    }
                }
            }

            if (com == null) {
                com = this.command;
            }

            if (com == null) {
                this.displayUsage(sender, label, subCommand);
                return true;
            } else if (com.canBeConsole() || !(sender instanceof ConsoleCommandSender) && !(sender instanceof RemoteConsoleCommandSender)) {
                if (!com.canBeCommandBlock() && sender instanceof BlockCommandSender) {
                    if (com == this.command) {
                        this.displayUsage(sender, label, subCommand);
                    } else {
                        sender.sendMessage(ChatUtils.t("noCommandBlock", label + " " + subCommand));
                    }

                    return true;
                } else if (com.getPermission() != null && !sender.hasPermission(com.getPermission())) {
                    StringBuilder stringBuilder = new StringBuilder(label);
                    stringBuilder.append(" ");
                    stringBuilder.append(subCommand);
                    String[] var16 = subArgs;
                    int var17 = subArgs.length;

                    for(int var18 = 0; var18 < var17; ++var18) {
                        String string = var16[var18];
                        stringBuilder.append(" ");
                        stringBuilder.append(string);
                    }

                    sender.sendMessage(ChatUtils.t("noPermission", com.getPermission(), stringBuilder.toString()));
                    return true;
                } else {
                    if (!com.onCommand(sender, subCommand, subArgs)) {
                        sender.sendMessage(ChatUtils.t("incorrectFormat", "/" + this.commandName + " " + com.getUsageString(subCommand, sender)));
                    }

                    return true;
                }
            } else {
                if (com == this.command) {
                    this.displayUsage(sender, label, subCommand);
                } else {
                    sender.sendMessage(ChatUtils.t("noConsole", label + " " + subCommand));
                }

                return true;
            }
        }
    }

    private void displayUsage(CommandSender sender, String label, String subCommand) {
        Map<String, ICommand> commands = LobbySwitch.p.getCommandManager().getCommandDispatcher().getCommands(sender);
        if (commands.isEmpty()) {
            sender.sendMessage(ChatUtils.t("noCommands"));
        } else {
            sender.sendMessage(ChatColor.DARK_GREEN + "» " + ChatColor.GREEN + "LobbySwitch Commands");
            Iterator var5 = commands.entrySet().iterator();

            while(var5.hasNext()) {
                Entry<String, ICommand> entry = (Entry)var5.next();
                sender.sendMessage(ChatColor.GREEN + "/" + label + " " + (String)entry.getKey() + ChatColor.GRAY + " " + ((ICommand)entry.getValue()).getDescription());
            }
        }

    }

    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        List<String> results = new ArrayList();
        if (args.length == 1) {
            Iterator var6 = this.subCommands.values().iterator();

            while(true) {
                ICommand registeredCommand;
                do {
                    do {
                        do {
                            if (!var6.hasNext()) {
                                return (List)results;
                            }

                            registeredCommand = (ICommand)var6.next();
                        } while(!registeredCommand.getName().toLowerCase().startsWith(args[0].toLowerCase()));
                    } while(!registeredCommand.canBeConsole() && (sender instanceof ConsoleCommandSender || sender instanceof RemoteConsoleCommandSender));
                } while(registeredCommand.getPermission() != null && !sender.hasPermission(registeredCommand.getPermission()));

                ((List)results).add(registeredCommand.getName());
            }
        } else {
            String subCommand = args[0].toLowerCase();
            String[] subArgs = args.length > 1 ? (String[])Arrays.copyOfRange(args, 1, args.length) : new String[0];
            ICommand com = null;
            if (this.subCommands.containsKey(subCommand)) {
                com = (ICommand)this.subCommands.get(subCommand);
            } else {
                Iterator var9 = this.subCommands.entrySet().iterator();

                label91:
                while(var9.hasNext()) {
                    Entry<String, ICommand> ent = (Entry)var9.next();
                    if (((ICommand)ent.getValue()).getAliases() != null) {
                        String[] aliases = ((ICommand)ent.getValue()).getAliases();
                        String[] var12 = aliases;
                        int var13 = aliases.length;

                        for(int var14 = 0; var14 < var13; ++var14) {
                            String alias = var12[var14];
                            if (subCommand.equalsIgnoreCase(alias)) {
                                com = (ICommand)ent.getValue();
                                break label91;
                            }
                        }
                    }
                }
            }

            if (com == null) {
                return (List)results;
            } else if (!com.canBeConsole() && (sender instanceof ConsoleCommandSender || sender instanceof RemoteConsoleCommandSender)) {
                return (List)results;
            } else if (com.getPermission() != null && !sender.hasPermission(com.getPermission())) {
                return (List)results;
            } else {
                results = com.onTabComplete(sender, subCommand, subArgs);
                if (results == null) {
                    return new ArrayList();
                } else {
                    return (List)results;
                }
            }
        }
    }

    public String getCommandHelp() {
        return this.commandName + this.commandDescription;
    }

    public Map<String, ICommand> getCommands(CommandSender commandSender) {
        Map<String, ICommand> commands = new LinkedHashMap();
        commands.put("", this.command);
        commands.putAll(this.subCommands);
        List<String> toRemove = new ArrayList();
        Iterator var4 = commands.keySet().iterator();

        String string;
        while(var4.hasNext()) {
            string = (String)var4.next();
            ICommand iCommand = (ICommand)commands.get(string);
            if (iCommand.getPermission() != null && !commandSender.hasPermission(iCommand.getPermission())) {
                toRemove.add(string);
            }
        }

        var4 = toRemove.iterator();

        while(var4.hasNext()) {
            string = (String)var4.next();
            commands.remove(string);
        }

        return commands;
    }
}
