package com.lobbyswitch.versions;

import com.lobbyswitch.config.ConfigPaths;
import java.util.Iterator;
import org.bukkit.configuration.file.FileConfiguration;

public abstract class Config0_3_8 {
    public static FileConfiguration update(FileConfiguration fileConfiguration) {
        Iterator var1 = fileConfiguration.getConfigurationSection("Server_Slots").getKeys(false).iterator();

        while(var1.hasNext()) {
            String string = (String)var1.next();
            fileConfiguration.set(ConfigPaths.getSlotPath("Server_Slots.Slot.MetaData", Integer.valueOf(string)), 0);
        }

        fileConfiguration.set("Version", "0.3.8");
        return fileConfiguration;
    }

    public static boolean equals(FileConfiguration fileConfiguration) {
        return fileConfiguration.contains("Version") && (fileConfiguration.getString("Version").equals("0.3.8") || fileConfiguration.getString("Version").equals("0.3.9"));
    }
}
