package com.lobbyswitch.command.commands;

import com.lobbyswitch.LobbySwitch;
import com.lobbyswitch.ServerItem;
import com.lobbyswitch.command.ICommand;
import com.lobbyswitch.util.ChatUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.ItemStack;

public class AddCommand implements ICommand {
    public String getName() {
        return "add";
    }

    public String[] getAliases() {
        return new String[]{"a"};
    }

    public String getPermission() {
        return "lobbyswitch.add";
    }

    public String getUsageString(String label, CommandSender sender) {
        return label + " <Item Name:Item ID:Meta Data> <Amount> <Enchanted> <Slot> <Target Server> <Display Name>";
    }

    public String getDescription() {
        return "Add a new item to the selector GUI that will connect you to a specified server.";
    }

    public boolean canBeConsole() {
        return true;
    }

    public boolean canBeCommandBlock() {
        return true;
    }

    public boolean onCommand(CommandSender sender, String label, String... args) {
        if (args.length <= 5) {
            return false;
        } else {
            try {
                int amount = Integer.parseInt(args[1]);
                if (amount > 64) {
                    sender.sendMessage(ChatUtils.t("amountMaxExceeded", "64"));
                    return false;
                } else if (amount < 1) {
                    sender.sendMessage(ChatUtils.t("amountMinExceeded", "0"));
                    return false;
                } else {
                    ItemStack itemStack;
                    try {
                        itemStack = new ItemStack(Material.matchMaterial(args[0]), amount);
                    } catch (IllegalArgumentException var13) {
                        sender.sendMessage(ChatUtils.t("invalidItem", args[0]));
                        return false;
                    }

                    if (itemStack.getType() == Material.AIR) {
                        sender.sendMessage(ChatUtils.t("invalidItem", "AIR"));
                        return false;
                    } else if (!args[2].equalsIgnoreCase("true") && !args[2].equalsIgnoreCase("false")) {
                        sender.sendMessage(ChatUtils.t("invalidBoolean", args[2]));
                        return false;
                    } else {
                        boolean enchanted = Boolean.parseBoolean(args[2]);

                        int slot;
                        try {
                            slot = Integer.parseInt(args[3]);
                            if (slot < 1) {
                                sender.sendMessage(ChatUtils.t("slotMinExceeded", "0"));
                                return false;
                            }

                            if (slot > LobbySwitch.p.getConfigManager().getInventory().getSize()) {
                                sender.sendMessage(ChatUtils.t("slotMaxExceeded", LobbySwitch.p.getConfigManager().getInventory().getSize() + ""));
                                return false;
                            }

                            if (LobbySwitch.p.getConfigManager().getSlots().contains(String.valueOf(slot))) {
                                sender.sendMessage(ChatUtils.t("slotInUse", slot + ""));
                                return false;
                            }
                        } catch (NumberFormatException var15) {
                            sender.sendMessage(ChatUtils.t("invalidInteger", args[3]));
                            return true;
                        }

                        if (LobbySwitch.p.getServers().keySet().contains(args[4])) {
                            String targetServer = args[4];
                            StringBuilder stringBuilder = new StringBuilder();

                            for(int i = 5; args.length > i; ++i) {
                                if (i != 5) {
                                    stringBuilder.append(" ");
                                }

                                stringBuilder.append(args[i]);
                            }

                            ServerItem serverItem = new ServerItem(itemStack.getType(), String.valueOf(itemStack.getAmount()), stringBuilder.toString(), targetServer, new ArrayList<>(), enchanted);
                            LobbySwitch.p.getConfigManager().saveServerItem(serverItem, slot);
                            sender.sendMessage(ChatUtils.t("itemCreated", slot + "", serverItem.getAmount(), serverItem.getDisplayName(), serverItem.getMaterial().toString(), serverItem.getTargetServer(), serverItem.isEnchanted() + ""));
                            return true;
                        } else {
                            sender.sendMessage(ChatUtils.t("invalidServer", args[4]));
                            return false;
                        }
                    }
                }
            } catch (NumberFormatException var16) {
                sender.sendMessage(ChatUtils.t("invalidInteger", args[1]));
                return false;
            }
        }
    }

    public List<String> onTabComplete(CommandSender sender, String label, String... args) {
        List<String> matches = new ArrayList<>();
        String search = args[args.length - 1].toLowerCase();
        int i;
        if (args.length == 1) {
            Material[] var6 = Material.values();
            i = var6.length;

            for(int var8 = 0; var8 < i; ++var8) {
                Material material = var6[var8];
                if (material.name().toLowerCase().startsWith(search)) {
                    matches.add(material.name());
                }
            }
        }

        if (args.length == 2) {
            for(int j = 1; 64 >= j; ++j) {
                if (String.valueOf(j).toLowerCase().startsWith(search)) {
                    matches.add(String.valueOf(j));
                }
            }
        }

        if (args.length == 3) {
            if ("true".startsWith(search)) {
                matches.add("true");
            }

            if ("false".startsWith(search)) {
                matches.add("false");
            }
        }

        if (args.length == 4) {
            ArrayList<String> used = new ArrayList<>(LobbySwitch.p.getConfigManager().getSlots());

            for(i = 1; LobbySwitch.p.getConfigManager().getInventory().getSize() >= i; ++i) {
                if (String.valueOf(i).toLowerCase().startsWith(search) && !used.contains(String.valueOf(i))) {
                    matches.add(String.valueOf(i));
                }
            }
        }

        if (args.length == 5) {
            Iterator var12 = LobbySwitch.p.getServers().keySet().iterator();

            while(var12.hasNext()) {
                String string = (String)var12.next();
                if (string.toLowerCase().startsWith(search)) {
                    matches.add(string);
                }
            }
        }

        return matches;
    }
}
