package com.lobbyswitch.config;

import com.lobbyswitch.LobbySwitch;
import com.lobbyswitch.versions.Config0_2_1;
import com.lobbyswitch.versions.Config0_2_2;
import com.lobbyswitch.versions.Config0_3_5;
import com.lobbyswitch.versions.Config0_3_8;
import com.lobbyswitch.versions.Config0_4;
import com.lobbyswitch.versions.Config0_4_9;
import com.lobbyswitch.versions.Config0_5_3;
import com.lobbyswitch.versions.Config0_5_4;
import org.bukkit.configuration.file.FileConfiguration;

public class ConfigUpdater {
    private FileConfiguration fileConfiguration;

    public ConfigUpdater(FileConfiguration fileConfiguration) {
        this.fileConfiguration = fileConfiguration;
    }

    public void update() {
        if (Config0_2_1.equals(this.fileConfiguration)) {
            this.fileConfiguration = Config0_2_2.update(this.fileConfiguration);
        }

        if (Config0_2_2.equals(this.fileConfiguration)) {
            this.fileConfiguration = Config0_3_5.update(this.fileConfiguration);
        }

        if (Config0_3_5.equals(this.fileConfiguration)) {
            this.fileConfiguration = Config0_3_8.update(this.fileConfiguration);
        }

        if (Config0_3_8.equals(this.fileConfiguration)) {
            this.fileConfiguration = Config0_4.update(this.fileConfiguration);
        }

        if (Config0_4.equals(this.fileConfiguration)) {
            this.fileConfiguration = Config0_4_9.update(this.fileConfiguration);
        }

        if (Config0_4_9.equals(this.fileConfiguration)) {
            this.fileConfiguration = Config0_5_3.update(this.fileConfiguration);
        }

        if (Config0_5_3.equals(this.fileConfiguration)) {
            this.fileConfiguration = Config0_5_4.update(this.fileConfiguration);
        }

        LobbySwitch.p.saveConfig();
    }
}
