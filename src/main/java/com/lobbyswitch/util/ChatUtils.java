package com.lobbyswitch.util;

import com.lobbyswitch.LobbySwitch;

public class ChatUtils {
    public static String t(String string, String... values) {
        String message = LobbySwitch.p.getMessages().getString(string);

        for(int index = 0; message.contains("{" + index + "}"); ++index) {
            message = message.replace("{" + index + "}", values[index]).replace("§", "§");
        }

        return message;
    }

    public static String replaceLowerCasePlaceholder(String text, String toReplace, Object replaceWith) {
        int startIndex = text.toLowerCase().indexOf(toReplace.toLowerCase());
        int endIndex = startIndex + toReplace.length();
        return startIndex == -1 ? text : text.substring(0, startIndex) + replaceWith + text.substring(endIndex, text.length());
    }
}
