package com.lobbyswitch.command.commands;

import com.lobbyswitch.LobbySwitch;
import com.lobbyswitch.ServerItem;
import com.lobbyswitch.command.ICommand;
import com.lobbyswitch.util.ChatUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;

public class EditCommand implements ICommand {
    private final List<String> editSubArguments = new ArrayList<String>() {
        {
            this.add("amount");
            this.add("enchanted");
            this.add("lore");
            this.add("material");
            this.add("name");
            this.add("slot");
            this.add("target");
        }
    };

    public String getName() {
        return "edit";
    }

    public String[] getAliases() {
        return new String[]{"e"};
    }

    public String getPermission() {
        return "lobbyswitch.edit";
    }

    public String getUsageString(String label, CommandSender sender) {
        return label + " <Slot> <Amount:Lore:Material:Name:Slot:Target Server> <New Amount:New Item Name:New Item ID:New Meta Data:New Name:New Slot:New Target>";
    }

    public String getDescription() {
        return "Modify items in the selector GUI.";
    }

    public boolean canBeConsole() {
        return true;
    }

    public boolean canBeCommandBlock() {
        return true;
    }

    public boolean onCommand(CommandSender sender, String label, String... args) {
        if (args.length < 3) {
            return false;
        } else {
            int slot;
            try {
                slot = Integer.parseInt(args[0]);
                if (slot < 1) {
                    sender.sendMessage(ChatUtils.t("slotMinExceeded", "0"));
                    return false;
                }

                if (slot > LobbySwitch.p.getConfigManager().getInventory().getSize()) {
                    sender.sendMessage(ChatUtils.t("slotMaxExceeded", LobbySwitch.p.getConfigManager().getInventory().getSize() + ""));
                    return false;
                }

                if (!LobbySwitch.p.getConfigManager().getSlots().contains(String.valueOf(slot))) {
                    sender.sendMessage(ChatUtils.t("noItem", slot + ""));
                    return false;
                }
            } catch (NumberFormatException var22) {
                sender.sendMessage(ChatUtils.t("invalidInteger", args[0]));
                return false;
            }

            ServerItem serverItem = LobbySwitch.p.getConfigManager().getServerItem(slot);
            String var6 = args[1].toLowerCase();
            byte var7 = -1;
            switch(var6.hashCode()) {
            case -1413853096:
                if (var6.equals("amount")) {
                    var7 = 0;
                }
                break;
            case -880905839:
                if (var6.equals("target")) {
                    var7 = 4;
                }
                break;
            case 3327734:
                if (var6.equals("lore")) {
                    var7 = 6;
                }
                break;
            case 3373707:
                if (var6.equals("name")) {
                    var7 = 5;
                }
                break;
            case 3533310:
                if (var6.equals("slot")) {
                    var7 = 3;
                }
                break;
            case 299066663:
                if (var6.equals("material")) {
                    var7 = 2;
                }
                break;
            case 1305257656:
                if (var6.equals("enchanted")) {
                    var7 = 1;
                }
            }

            StringBuilder stringBuilder;
            int i;
            switch(var7) {
            case 0:
                String amount;
                try {
                    amount = String.valueOf(Integer.parseInt(args[2]));
                    if (Integer.valueOf(amount) > 64) {
                        sender.sendMessage(ChatUtils.t("amountMaxExceeded", "64"));
                        return false;
                    }

                    if (Integer.valueOf(amount) < 1) {
                        sender.sendMessage(ChatUtils.t("amountMinExceeded", "0"));
                        return false;
                    }
                } catch (NumberFormatException var21) {
                    if (!args[2].equalsIgnoreCase("%PLAYER_COUNT%")) {
                        sender.sendMessage(ChatUtils.t("invalidInteger", args[2]));
                        return false;
                    }

                    amount = "%PLAYER_COUNT%";
                }

                serverItem.setAmount(amount);
                LobbySwitch.p.getConfigManager().saveServerItem(serverItem, slot);
                sender.sendMessage(ChatUtils.t("setAmount", amount));
                return true;
            case 1:
                if (!args[2].equalsIgnoreCase("true") && !args[2].equalsIgnoreCase("false")) {
                    sender.sendMessage(ChatUtils.t("invalidBoolean", args[2]));
                    return false;
                }

                boolean enchanted = Boolean.parseBoolean(args[2]);
                serverItem.setEnchanted(enchanted);
                LobbySwitch.p.getConfigManager().saveServerItem(serverItem, slot);
                sender.sendMessage(ChatUtils.t("setEnchanted", enchanted + ""));
                return true;
            case 2:
                Material material = Material.matchMaterial(args[2]);
                if (material == null) {
                    sender.sendMessage(ChatUtils.t("invalidItem", args[2]));
                    return false;
                }

                if (material == Material.AIR) {
                    sender.sendMessage(ChatUtils.t("invalidItem", "AIR"));
                    return false;
                }

                serverItem.setMaterial(material);
                LobbySwitch.p.getConfigManager().saveServerItem(serverItem, slot);
                sender.sendMessage(ChatUtils.t("setMaterial", material.name()));
                return true;
            case 3:
                try {
                    int newSlot = Integer.parseInt(args[2]);
                    if (newSlot < 1) {
                        sender.sendMessage(ChatUtils.t("slotMinExceeded", "0"));
                        return false;
                    } else if (newSlot > LobbySwitch.p.getConfigManager().getInventory().getSize()) {
                        sender.sendMessage(ChatUtils.t("slotMaxExceeded", LobbySwitch.p.getConfigManager().getInventory().getSize() + ""));
                        return false;
                    } else if (slot == newSlot) {
                        sender.sendMessage(ChatUtils.t("sameSlot"));
                        return false;
                    } else {
                        if (LobbySwitch.p.getConfigManager().getSlots().contains(String.valueOf(newSlot))) {
                            sender.sendMessage(ChatUtils.t("slotInUse", newSlot + ""));
                            return false;
                        }

                        LobbySwitch.p.getConfigManager().removeSlot(slot);
                        LobbySwitch.p.getConfigManager().saveServerItem(serverItem, newSlot);
                        sender.sendMessage(ChatUtils.t("setSlot", slot + "", newSlot + ""));
                        return true;
                    }
                } catch (NumberFormatException var18) {
                    sender.sendMessage(ChatUtils.t("invalidInteger", args[2]));
                    return false;
                }
            case 4:
                if (LobbySwitch.p.getServers().keySet().contains(args[3])) {
                    String targetServer = args[2];
                    serverItem.setTargetServer(targetServer);
                    LobbySwitch.p.getConfigManager().saveServerItem(serverItem, slot);
                    sender.sendMessage(ChatUtils.t("setTarget", targetServer));
                    return true;
                }

                sender.sendMessage(ChatUtils.t("invalidServer", args[2]));
                return false;
            case 5:
                stringBuilder = new StringBuilder();

                for(i = 2; args.length > i; ++i) {
                    if (i != 2) {
                        stringBuilder.append(" ");
                    }

                    stringBuilder.append(args[i]);
                }

                serverItem.setDisplayName(stringBuilder.toString());
                LobbySwitch.p.getConfigManager().saveServerItem(serverItem, slot);
                sender.sendMessage(ChatUtils.t("setDisplayName", stringBuilder.toString()));
                return true;
            case 6:
                stringBuilder = new StringBuilder();

                for(i = 2; args.length > i; ++i) {
                    if (i != 2) {
                        stringBuilder.append(" ");
                    }

                    stringBuilder.append(args[i]);
                }

                serverItem.setLore(Arrays.asList(stringBuilder.toString().split("/n")));
                LobbySwitch.p.getConfigManager().saveServerItem(serverItem, slot);
                sender.sendMessage(ChatUtils.t("setLore"));
                Iterator var23 = serverItem.getLore().iterator();

                while(var23.hasNext()) {
                    String string = (String)var23.next();
                    sender.sendMessage("    " + ChatColor.WHITE + string.replace("&", "§"));
                }

                return true;
            default:
                return false;
            }
        }
    }

    public List<String> onTabComplete(CommandSender sender, String label, String... args) {
        List<String> matches = new ArrayList();
        String search = args[args.length - 1].toLowerCase();
        Iterator var6;
        String string;
        if (args.length == 1) {
            var6 = LobbySwitch.p.getConfigManager().getSlots().iterator();

            while(var6.hasNext()) {
                string = (String)var6.next();
                if (string.toLowerCase().startsWith(search)) {
                    matches.add(string);
                }
            }
        }

        if (args.length == 2) {
            var6 = this.editSubArguments.iterator();

            while(var6.hasNext()) {
                string = (String)var6.next();
                if (string.toLowerCase().startsWith(search)) {
                    matches.add(string);
                }
            }
        }

        if (args.length == 3) {
            if (args[1].equalsIgnoreCase("amount")) {
                for(int i = 1; 64 >= i; ++i) {
                    if (String.valueOf(i).toLowerCase().startsWith(search)) {
                        matches.add(String.valueOf(i));
                    }
                }
            }

            if (args[1].equalsIgnoreCase("enchanted")) {
                if ("true".startsWith(search)) {
                    matches.add("true");
                }

                if ("false".startsWith(search)) {
                    matches.add("false");
                }
            }

            int i;
            if (args[1].equalsIgnoreCase("material")) {
                Material[] var11 = Material.values();
                i = var11.length;

                for(int var8 = 0; var8 < i; ++var8) {
                    Material argument = var11[var8];
                    if (argument.name().toLowerCase().startsWith(search)) {
                        matches.add(argument.name());
                    }
                }
            }

            if (args[1].equalsIgnoreCase("slot")) {
                ArrayList<String> used = new ArrayList(LobbySwitch.p.getConfigManager().getSlots());

                for(i = 1; LobbySwitch.p.getConfigManager().getInventory().getSize() >= i; ++i) {
                    if (String.valueOf(i).toLowerCase().startsWith(search) && !used.contains(String.valueOf(i))) {
                        matches.add(String.valueOf(i));
                    }
                }
            }

            if (args[1].equalsIgnoreCase("target")) {
                var6 = LobbySwitch.p.getServers().keySet().iterator();

                while(var6.hasNext()) {
                    string = (String)var6.next();
                    if (string.toLowerCase().startsWith(search)) {
                        matches.add(string);
                    }
                }
            }
        }

        return matches;
    }
}
