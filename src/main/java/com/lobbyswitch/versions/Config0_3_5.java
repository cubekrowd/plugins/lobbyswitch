package com.lobbyswitch.versions;

import com.lobbyswitch.ServerItem;
import com.lobbyswitch.config.ConfigPaths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;

public abstract class Config0_3_5 {
    public static FileConfiguration update(FileConfiguration fileConfiguration) {
        int rows = fileConfiguration.getInt("InventoryRows");
        String inventory_name = fileConfiguration.getString("InventoryName");
        Material material = fileConfiguration.getItemStack("ItemStack").getType();
        String selector_name = fileConfiguration.getItemStack("ItemStack").getItemMeta().getDisplayName();
        HashMap<Integer, ServerItem> serverItems = new HashMap();
        ArrayList<String> serverList = new ArrayList(fileConfiguration.getStringList("Servers"));
        Iterator var7 = serverList.iterator();

        while(var7.hasNext()) {
            String string = (String)var7.next();
            String[] split = string.split(":");
            serverItems.put(Integer.parseInt(split[5]), new ServerItem(Material.valueOf(split[0]), String.valueOf(Integer.parseInt(split[1])), split[2], split[3], new ArrayList(), false));
        }

        String version = fileConfiguration.getString("Version");
        fileConfiguration.set("Inventory.Rows", rows);
        fileConfiguration.set("Inventory.Name", inventory_name);
        fileConfiguration.set("Selector.Material", material.name());
        fileConfiguration.set("Selector.Display_Name", "&4" + ChatColor.stripColor(selector_name));
        Iterator var12 = serverItems.keySet().iterator();

        while(var12.hasNext()) {
            Integer slot = (Integer)var12.next();
            ServerItem serverItem = (ServerItem)serverItems.get(slot);
            fileConfiguration.set(ConfigPaths.getSlotPath("Server_Slots.Slot.Amount", slot), serverItem.getAmount());
            fileConfiguration.set(ConfigPaths.getSlotPath("Server_Slots.Slot.Display_Name", slot), "&a" + serverItem.getDisplayName());
            fileConfiguration.set(ConfigPaths.getSlotPath("Server_Slots.Slot.Material", slot), serverItem.getMaterial().name());
            fileConfiguration.set(ConfigPaths.getSlotPath("Server_Slots.Slot.Target_Server", slot), serverItem.getTargetServer());
        }

        fileConfiguration.set("Version", version);
        fileConfiguration.set("InventoryRows", (Object)null);
        fileConfiguration.set("InventoryName", (Object)null);
        fileConfiguration.set("ItemStack", (Object)null);
        fileConfiguration.set("Servers", (Object)null);
        fileConfiguration.set("Version", "0.3.5");
        return fileConfiguration;
    }

    public static boolean equals(FileConfiguration fileConfiguration) {
        return fileConfiguration.contains("Version") && (fileConfiguration.getString("Version").equals("0.3.5") || fileConfiguration.getString("Version").equals("0.3.6") || fileConfiguration.get("Version").equals("0.3.7"));
    }
}
