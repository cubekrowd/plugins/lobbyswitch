package com.lobbyswitch;

import com.lobbyswitch.util.ItemUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ServerItem {
    private Material material;
    private String amount;
    private String displayName;
    private String targetServer;
    private List<String> lore;
    private boolean enchanted;

    public ServerItem(Material material, String amount, String displayName, String targetServer, List<String> lore, boolean enchanted) {
        this.material = material;
        this.amount = amount;
        this.displayName = displayName;
        this.targetServer = targetServer;
        this.lore = lore;
        this.enchanted = enchanted;
    }

    public ItemStack getItemStack() {
        int intAmount;
        try {
            intAmount = Integer.parseInt(this.amount);
        } catch (NumberFormatException var7) {
            intAmount = LobbySwitch.p.getServers().get(this.targetServer).getPlayerCount();
        }

        ItemStack itemStack = new ItemStack(this.material, intAmount);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(this.displayName.replace("&", "§"));
        List<String> loreFormatted = new ArrayList<>();

        for (var loreLine : lore) {
            loreFormatted.add(loreLine.replace("&", "§"));
        }

        itemMeta.setLore(loreFormatted);

        itemMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_DESTROYS,
                ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_PLACED_ON,
                ItemFlag.HIDE_POTION_EFFECTS, ItemFlag.HIDE_UNBREAKABLE);
        itemStack.setItemMeta(itemMeta);

        if (this.enchanted) {
            ItemUtil.addGlow(itemStack);
        }
        return itemStack;
    }

    public Material getMaterial() {
        return this.material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public String getAmount() {
        return this.amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getTargetServer() {
        return this.targetServer;
    }

    public void setTargetServer(String targetServer) {
        this.targetServer = targetServer;
    }

    public List<String> getLore() {
        return this.lore;
    }

    public void setLore(List<String> lore) {
        this.lore = lore;
    }

    public boolean isEnchanted() {
        return this.enchanted;
    }

    public void setEnchanted(boolean enchanted) {
        this.enchanted = enchanted;
    }
}
