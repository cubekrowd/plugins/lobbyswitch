package com.lobbyswitch.config;

import com.lobbyswitch.LobbySwitch;
import com.lobbyswitch.ServerData;
import com.lobbyswitch.ServerItem;
import com.lobbyswitch.util.ChatUtils;
import com.lobbyswitch.util.ItemUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ConfigManager {
    private FileConfiguration fileConfiguration;

    public ConfigManager(FileConfiguration fileConfiguration) {
        this.fileConfiguration = fileConfiguration;
    }

    public FileConfiguration getFileConfiguration() {
        return this.fileConfiguration;
    }

    public ItemStack getSelector() {
        ItemStack itemStack = new ItemStack(Material.matchMaterial((String)this.fileConfiguration.get("Selector.Material")), 1);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(this.fileConfiguration.getString("Selector.Display_Name").replace("&", "§"));

        itemMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_DESTROYS,
                ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_PLACED_ON,
                ItemFlag.HIDE_POTION_EFFECTS, ItemFlag.HIDE_UNBREAKABLE);
        itemStack.setItemMeta(itemMeta);

        if (this.fileConfiguration.getBoolean("Server_Slots.Slot.Enchanted")) {
            ItemUtil.addGlow(itemStack);
        }

        return itemStack;
    }

    public Inventory getInventory() {
        Inventory inventory = Bukkit.createInventory((InventoryHolder)null, 9 * this.fileConfiguration.getInt("Inventory.Rows"), this.fileConfiguration.getString("Inventory.Name").replace("&", "§"));
        Iterator var2 = LobbySwitch.p.getConfigManager().getSlots().iterator();

        while(true) {
            String string;
            ServerItem serverItem;
            do {
                if (!var2.hasNext()) {
                    return inventory;
                }

                string = (String)var2.next();
                serverItem = LobbySwitch.p.getConfigManager().getServerItem(Integer.parseInt(string));
            } while(serverItem == null);

            ItemStack serverItemStack = serverItem.getItemStack();
            if (LobbySwitch.p.getServers().keySet().contains(serverItem.getTargetServer())) {
                ServerData serverData = (ServerData)LobbySwitch.p.getServers().get(serverItem.getTargetServer());
                if (serverItemStack.getItemMeta() != null) {
                    ItemMeta itemMeta = serverItemStack.getItemMeta();
                    String displayName = itemMeta.getDisplayName();
                    displayName = ChatUtils.replaceLowerCasePlaceholder(displayName, "%PLAYER_COUNT%", serverData.getPlayerCount());
                    displayName = ChatUtils.replaceLowerCasePlaceholder(displayName, "%TARGET_MOTD%", serverData.getMOTD());
                    itemMeta.setDisplayName(displayName);
                    List<String> loreLines = new ArrayList();
                    if (itemMeta.getLore() != null) {
                        Iterator var10 = itemMeta.getLore().iterator();

                        while(var10.hasNext()) {
                            String loreLine = (String)var10.next();
                            loreLine = ChatUtils.replaceLowerCasePlaceholder(loreLine, "%PLAYER_COUNT%", serverData.getPlayerCount());
                            loreLine = ChatUtils.replaceLowerCasePlaceholder(loreLine, "%TARGET_MOTD%", serverData.getMOTD());
                            if (loreLine.contains("\n")) {
                                loreLines.addAll(Arrays.asList(loreLine.split("\n")));
                            } else {
                                loreLines.add(loreLine);
                            }
                        }
                    }

                    itemMeta.setLore(loreLines);
                    serverItemStack.setItemMeta(itemMeta);
                }
            }

            inventory.setItem(Integer.parseInt(string) - 1, serverItemStack);
        }
    }

    public Set<String> getSlots() {
        return this.fileConfiguration.getConfigurationSection("Server_Slots").getKeys(false);
    }

    public ServerItem getServerItem(int slot) {
        if (this.getSlots().contains(String.valueOf(slot))) {
            String amount = this.fileConfiguration.getString(ConfigPaths.getSlotPath("Server_Slots.Slot.Amount", slot));
            String displayName = this.fileConfiguration.getString(ConfigPaths.getSlotPath("Server_Slots.Slot.Display_Name", slot));
            Material material = Material.matchMaterial(this.fileConfiguration.getString(ConfigPaths.getSlotPath("Server_Slots.Slot.Material", slot)));
            String targetServer = this.fileConfiguration.getString(ConfigPaths.getSlotPath("Server_Slots.Slot.Target_Server", slot));
            List<String> lore = this.fileConfiguration.getStringList(ConfigPaths.getSlotPath("Server_Slots.Slot.Lore", slot));
            boolean enchanted = this.fileConfiguration.getBoolean(ConfigPaths.getSlotPath("Server_Slots.Slot.Enchanted", slot));
            return new ServerItem(material, amount, displayName, targetServer, lore, enchanted);
        } else {
            return null;
        }
    }

    public void saveServerItem(ServerItem serverItem, int slot) {
        this.fileConfiguration.set(ConfigPaths.getSlotPath("Server_Slots.Slot.Amount", slot), serverItem.getAmount());
        this.fileConfiguration.set(ConfigPaths.getSlotPath("Server_Slots.Slot.Display_Name", slot), serverItem.getDisplayName());
        this.fileConfiguration.set(ConfigPaths.getSlotPath("Server_Slots.Slot.Material", slot), serverItem.getMaterial().name());
        this.fileConfiguration.set(ConfigPaths.getSlotPath("Server_Slots.Slot.Target_Server", slot), serverItem.getTargetServer());
        this.fileConfiguration.set(ConfigPaths.getSlotPath("Server_Slots.Slot.Lore", slot), serverItem.getLore());
        this.fileConfiguration.set(ConfigPaths.getSlotPath("Server_Slots.Slot.Enchanted", slot), serverItem.isEnchanted());
        LobbySwitch.p.saveConfig();
    }

    public boolean removeSlot(int slot) {
        if (this.getSlots().contains(String.valueOf(slot))) {
            this.fileConfiguration.set(ConfigPaths.getSlotPath("Server_Slots.Slot", slot), (Object)null);
            LobbySwitch.p.saveConfig();
            return true;
        } else {
            return false;
        }
    }
}
