package com.lobbyswitch.versions;

import org.bukkit.configuration.file.FileConfiguration;

public abstract class Config0_5_3 {
    public static FileConfiguration update(FileConfiguration fileConfiguration) {
        fileConfiguration.set("Selector.Add_On_Join", true);
        fileConfiguration.set("Selector.Droppable", false);
        fileConfiguration.set("Selector.Slot.Forced", false);
        fileConfiguration.set("Selector.Slot.Position", -1);
        fileConfiguration.set("Version", "0.5.3");
        return fileConfiguration;
    }

    public static boolean equals(FileConfiguration fileConfiguration) {
        return fileConfiguration.contains("Version") && fileConfiguration.getString("Version").equals("0.5.3");
    }
}
