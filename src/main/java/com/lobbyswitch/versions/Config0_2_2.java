package com.lobbyswitch.versions;

import java.util.ArrayList;
import org.bukkit.configuration.file.FileConfiguration;

public abstract class Config0_2_2 {
    public static FileConfiguration update(FileConfiguration fileConfiguration) {
        ArrayList<String> newServerList = new ArrayList();

        for(int i = 0; i < fileConfiguration.getList("Servers").size(); ++i) {
            String string = (String)fileConfiguration.getStringList("Servers").get(i);
            String split = string + ":" + (i + 1);
            newServerList.add(split);
        }

        fileConfiguration.set("Servers", newServerList);
        fileConfiguration.set("Version", "0.2.2");
        return fileConfiguration;
    }

    public static boolean equals(FileConfiguration fileConfiguration) {
        return fileConfiguration.contains("Version") && (fileConfiguration.getString("Version").equals("0.2.2") || fileConfiguration.getString("Version").equals("0.2.3") || fileConfiguration.getString("Version").equals("0.3"));
    }
}
