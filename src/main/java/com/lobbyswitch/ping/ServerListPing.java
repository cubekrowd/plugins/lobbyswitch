package com.lobbyswitch.ping;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.lobbyswitch.ping.impl.StatusResponse1_8;
import com.lobbyswitch.ping.impl.StatusResponse1_9;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public class ServerListPing {
    private InetSocketAddress host;
    private int timeout = 1500;
    private Gson gson = new Gson();

    public StatusResponse fetchData() throws IOException {
        Socket socket = new Socket();
        socket.setSoTimeout(this.timeout);
        socket.connect(this.host, this.timeout);
        OutputStream outputStream = socket.getOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
        InputStream inputStream = socket.getInputStream();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream handshake = new DataOutputStream(b);
        handshake.writeByte(0);
        this.writeVarInt(handshake, 4);
        this.writeVarInt(handshake, this.host.getHostString().length());
        handshake.writeBytes(this.host.getHostString());
        handshake.writeShort(this.host.getPort());
        this.writeVarInt(handshake, 1);
        this.writeVarInt(dataOutputStream, b.size());
        dataOutputStream.write(b.toByteArray());
        dataOutputStream.writeByte(1);
        dataOutputStream.writeByte(0);
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        this.readVarInt(dataInputStream);
        int id = this.readVarInt(dataInputStream);
        if (id == -1) {
            throw new IOException("Premature end of stream.");
        } else if (id != 0) {
            throw new IOException("Invalid packetID");
        } else {
            int length = this.readVarInt(dataInputStream);
            if (length == -1) {
                throw new IOException("Premature end of stream.");
            } else if (length == 0) {
                throw new IOException("Invalid string length.");
            } else {
                byte[] in = new byte[length];
                dataInputStream.readFully(in);
                String json = new String(in);
                long now = System.currentTimeMillis();
                dataOutputStream.writeByte(9);
                dataOutputStream.writeByte(1);
                dataOutputStream.writeLong(now);
                this.readVarInt(dataInputStream);
                id = this.readVarInt(dataInputStream);
                if (id == -1) {
                    throw new IOException("Premature end of stream.");
                } else if (id != 1) {
                    throw new IOException("Invalid packetID");
                } else {
                    long pingtime = dataInputStream.readLong();

                    StatusResponse response;
                    try {
                        response = (StatusResponse)this.gson.fromJson(json, StatusResponse1_9.class);
                    } catch (JsonSyntaxException var20) {
                        response = (StatusResponse)this.gson.fromJson(json, StatusResponse1_8.class);
                    }

                    response.setTime((int)(now - pingtime));
                    dataOutputStream.close();
                    outputStream.close();
                    inputStreamReader.close();
                    inputStream.close();
                    socket.close();
                    return response;
                }
            }
        }
    }

    public int readVarInt(DataInputStream in) throws IOException {
        int i = 0;
        int j = 0;

        byte k;
        do {
            k = in.readByte();
            i |= (k & 127) << j++ * 7;
            if (j > 5) {
                throw new RuntimeException("VarInt too big");
            }
        } while((k & 128) == 128);

        return i;
    }

    public void writeVarInt(DataOutputStream out, int paramInt) throws IOException {
        while((paramInt & -128) != 0) {
            out.writeByte(paramInt & 127 | 128);
            paramInt >>>= 7;
        }

        out.writeByte(paramInt);
    }

    public void setAddress(InetSocketAddress host) {
        this.host = host;
    }

    public InetSocketAddress getAddress() {
        return this.host;
    }

    void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    int getTimeout() {
        return this.timeout;
    }
}
