package com.lobbyswitch.command.commands;

import com.lobbyswitch.LobbySwitch;
import com.lobbyswitch.command.ICommand;
import com.lobbyswitch.util.ChatUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.bukkit.command.CommandSender;

public class RemoveCommand implements ICommand {
    public String getName() {
        return "remove";
    }

    public String[] getAliases() {
        return new String[]{"r"};
    }

    public String getPermission() {
        return "lobbyswitch.remove";
    }

    public String getUsageString(String label, CommandSender sender) {
        return label + " <Slot>";
    }

    public String getDescription() {
        return "Removes the item in a specified slot for the selector GUI.";
    }

    public boolean canBeConsole() {
        return true;
    }

    public boolean canBeCommandBlock() {
        return false;
    }

    public boolean onCommand(CommandSender sender, String label, String... args) {
        if (args.length != 1) {
            return false;
        } else {
            try {
                if (!LobbySwitch.p.getConfigManager().removeSlot(Integer.parseInt(args[0]))) {
                    sender.sendMessage(ChatUtils.t("serverNotFound", args[0]));
                    return false;
                } else {
                    sender.sendMessage(ChatUtils.t("removedSlot", args[0]));
                    return true;
                }
            } catch (NumberFormatException var5) {
                sender.sendMessage(ChatUtils.t("invalidInteger", args[0]));
                return false;
            }
        }
    }

    public List<String> onTabComplete(CommandSender sender, String label, String... args) {
        List<String> matches = new ArrayList();
        String search = args[args.length - 1].toLowerCase();
        if (args.length == 1) {
            Iterator var6 = LobbySwitch.p.getConfigManager().getSlots().iterator();

            while(var6.hasNext()) {
                String slot = (String)var6.next();
                if (slot.toLowerCase().startsWith(search)) {
                    matches.add(slot);
                }
            }
        }

        return matches;
    }
}
