package com.lobbyswitch.versions;

import com.lobbyswitch.config.ConfigPaths;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.bukkit.configuration.file.FileConfiguration;

public abstract class Config0_4 {
    public static FileConfiguration update(FileConfiguration fileConfiguration) {
        Iterator var1 = fileConfiguration.getConfigurationSection("Server_Slots").getKeys(false).iterator();

        while(var1.hasNext()) {
            String string = (String)var1.next();
            List<String> lore = Arrays.asList("&7%PLAYER_COUNT% &cOnline");
            fileConfiguration.set(ConfigPaths.getSlotPath("Server_Slots.Slot.Lore", Integer.valueOf(string)), lore);
        }

        fileConfiguration.set("Version", "0.4");
        return fileConfiguration;
    }

    public static boolean equals(FileConfiguration fileConfiguration) {
        return fileConfiguration.contains("Version") && (fileConfiguration.getString("Version").equals("0.4") || fileConfiguration.getString("Version").equals("0.4.1") || fileConfiguration.getString("Version").equals("0.4.2") || fileConfiguration.getString("Version").equals("0.4.3") || fileConfiguration.getString("Version").equals("0.4.4") || fileConfiguration.getString("Version").equals("0.4.5") || fileConfiguration.getString("Version").equals("0.4.6") || fileConfiguration.getString("Version").equals("0.4.7") || fileConfiguration.getString("Version").equals("0.4.8"));
    }
}
