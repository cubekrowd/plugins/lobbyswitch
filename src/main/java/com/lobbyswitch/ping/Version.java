package com.lobbyswitch.ping;

public class Version {
    private String name;
    private String protocol;

    public String getName() {
        return this.name;
    }

    public String getProtocol() {
        return this.protocol;
    }
}
