package com.lobbyswitch.ping;

public abstract class StatusResponse {
    private Players players;
    private Version version;
    private String favicon;
    private int time;

    public abstract String getDescription();

    public Players getPlayers() {
        return this.players;
    }

    public Version getVersion() {
        return this.version;
    }

    public String getFavicon() {
        return this.favicon;
    }

    public int getTime() {
        return this.time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
