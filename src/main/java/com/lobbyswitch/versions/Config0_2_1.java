package com.lobbyswitch.versions;

import java.util.Iterator;
import org.bukkit.configuration.file.FileConfiguration;

public abstract class Config0_2_1 {
    public static boolean equals(FileConfiguration fileConfiguration) {
        if (fileConfiguration.contains("InventoryName") && fileConfiguration.contains("InventoryRows") && fileConfiguration.contains("ItemStack") && fileConfiguration.contains("Servers") && fileConfiguration.contains("Version")) {
            boolean equalsVersion = false;
            Iterator var2 = fileConfiguration.getStringList("Servers").iterator();

            while(var2.hasNext()) {
                String string = (String)var2.next();
                String[] split = string.split(":");
                if (split.length == 5) {
                    equalsVersion = true;
                } else {
                    equalsVersion = false;
                }
            }

            return equalsVersion;
        } else {
            return false;
        }
    }
}
