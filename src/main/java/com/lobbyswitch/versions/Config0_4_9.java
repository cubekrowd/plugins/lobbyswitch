package com.lobbyswitch.versions;

import org.bukkit.configuration.file.FileConfiguration;

public abstract class Config0_4_9 {
    public static FileConfiguration update(FileConfiguration fileConfiguration) {
        fileConfiguration.set("MOTD_Refresh_Rate", fileConfiguration.getInt("Lore_Refresh_Rate"));
        fileConfiguration.set("Lore_Refresh_Rate", (Object)null);
        fileConfiguration.set("Version", "0.4.9");
        return fileConfiguration;
    }

    public static boolean equals(FileConfiguration fileConfiguration) {
        return fileConfiguration.contains("Version") && (fileConfiguration.getString("Version").equals("0.4.9") || fileConfiguration.getString("Version").equals("0.5") || fileConfiguration.getString("Version").equals("0.5.1") || fileConfiguration.getString("Version").equals("0.5.2"));
    }
}
