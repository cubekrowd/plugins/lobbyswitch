package com.lobbyswitch.command.commands;

import com.lobbyswitch.LobbySwitch;
import com.lobbyswitch.command.ICommand;
import com.lobbyswitch.util.ChatUtils;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class DefaultCommand implements ICommand {
    public String getName() {
        return "";
    }

    public String[] getAliases() {
        return new String[0];
    }

    public String getPermission() {
        return "lobbyswitch";
    }

    public String getUsageString(String label, CommandSender sender) {
        return label;
    }

    public String getDescription() {
        return "Shows a list of the available commands.";
    }

    public boolean canBeConsole() {
        return true;
    }

    public boolean canBeCommandBlock() {
        return false;
    }

    public boolean onCommand(CommandSender sender, String label, String... args) {
        sender.sendMessage(ChatColor.DARK_GREEN + "» " + ChatColor.GREEN + "LobbySwitch Commands");
        Iterator var4 = LobbySwitch.p.getCommandManager().getCommandDispatcher().getCommands(sender).entrySet().iterator();

        while(var4.hasNext()) {
            Entry<String, ICommand> entry = (Entry)var4.next();
            sender.sendMessage(ChatUtils.t("helpCommand", "lobbyswitch " + (String)entry.getKey(), ((ICommand)entry.getValue()).getDescription()));
        }

        return true;
    }

    public List<String> onTabComplete(CommandSender sender, String label, String... args) {
        return null;
    }
}
