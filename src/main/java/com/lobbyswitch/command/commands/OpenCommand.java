package com.lobbyswitch.command.commands;

import com.lobbyswitch.LobbySwitch;
import com.lobbyswitch.command.ICommand;
import com.lobbyswitch.util.ChatUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class OpenCommand implements ICommand {
    public String getName() {
        return "open";
    }

    public String[] getAliases() {
        return new String[]{"o"};
    }

    public String getPermission() {
        return "lobbyswitch.open";
    }

    public String getUsageString(String label, CommandSender sender) {
        return label + " <Player>";
    }

    public String getDescription() {
        return "Opens the selector GUI for a specified player";
    }

    public boolean canBeConsole() {
        return true;
    }

    public boolean canBeCommandBlock() {
        return true;
    }

    public boolean onCommand(CommandSender sender, String label, String... args) {
        if (args.length != 1) {
            return false;
        } else {
            Player player = Bukkit.getPlayer(args[0]);
            if (player != null && player.isOnline()) {
                player.openInventory(LobbySwitch.p.getConfigManager().getInventory());
                sender.sendMessage(ChatUtils.t("inventoryOpened", player.getName()));
                return true;
            } else {
                sender.sendMessage(ChatUtils.t("playerNotFound", args[0]));
                return false;
            }
        }
    }

    public List<String> onTabComplete(CommandSender sender, String label, String... args) {
        List<String> matches = new ArrayList();
        String search = args[args.length - 1].toLowerCase();
        if (args.length == 1) {
            Iterator var6 = Bukkit.getOnlinePlayers().iterator();

            while(var6.hasNext()) {
                Player player = (Player)var6.next();
                if (player.getName().toLowerCase().startsWith(search)) {
                    matches.add(player.getName());
                }
            }
        }

        return matches;
    }
}
