package com.lobbyswitch.command.commands;

import com.lobbyswitch.LobbySwitch;
import com.lobbyswitch.ServerItem;
import com.lobbyswitch.command.ICommand;
import com.lobbyswitch.util.ChatUtils;
import java.util.Iterator;
import java.util.List;
import org.bukkit.command.CommandSender;

public class ListCommand implements ICommand {
    public String getName() {
        return "list";
    }

    public String[] getAliases() {
        return new String[]{"l"};
    }

    public String getPermission() {
        return "lobbyswitch.list";
    }

    public String getUsageString(String label, CommandSender sender) {
        return label;
    }

    public String getDescription() {
        return "Displays information about the items in the selector GUI.";
    }

    public boolean canBeConsole() {
        return true;
    }

    public boolean canBeCommandBlock() {
        return true;
    }

    public boolean onCommand(CommandSender sender, String label, String... args) {
        if (args.length != 0) {
            return false;
        } else {
            sender.sendMessage(ChatUtils.t("serverList"));
            Iterator var4 = LobbySwitch.p.getConfigManager().getSlots().iterator();

            while(var4.hasNext()) {
                String string = (String)var4.next();
                ServerItem serverItem = LobbySwitch.p.getConfigManager().getServerItem(Integer.parseInt(string));
                sender.sendMessage(ChatUtils.t("serverListSlot", string));
                sender.sendMessage(ChatUtils.t("serverListSlotInfo", "Amount", serverItem.getAmount()));
                sender.sendMessage(ChatUtils.t("serverListSlotInfo", "Display Name", serverItem.getDisplayName()));
                sender.sendMessage(ChatUtils.t("serverListSlotInfo", "Material", serverItem.getMaterial().name()));
                sender.sendMessage(ChatUtils.t("serverListSlotInfo", "Target Server", serverItem.getTargetServer()));
                sender.sendMessage(ChatUtils.t("serverListSlotInfo", "Enchanted", serverItem.isEnchanted() + ""));
            }

            return true;
        }
    }

    public List<String> onTabComplete(CommandSender sender, String label, String... args) {
        return null;
    }
}
