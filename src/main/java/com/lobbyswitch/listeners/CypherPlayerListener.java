package com.lobbyswitch.listeners;

import com.lobbyswitch.LobbySwitch;
import com.lobbyswitch.config.ConfigManager;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.messaging.PluginMessageListener;

public class CypherPlayerListener implements Listener, PluginMessageListener {
    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        ConfigManager configManager = LobbySwitch.p.getConfigManager();
        if (!configManager.getFileConfiguration().getBoolean("Selector.Droppable") && event.getItemDrop().getItemStack().equals(configManager.getSelector())) {
            event.setCancelled(true);
        }

    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        LobbySwitch.p.getServer().getScheduler().scheduleSyncDelayedTask(LobbySwitch.p, new Runnable() {
            public void run() {
                var bytes = new ByteArrayOutputStream();
                var dataOutput = new DataOutputStream(bytes);
                try {
                    dataOutput.writeUTF("GetServers");
                    event.getPlayer().sendPluginMessage(LobbySwitch.p, "BungeeCord", bytes.toByteArray());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 20L);
        FileConfiguration fileConfiguration = LobbySwitch.p.getConfigManager().getFileConfiguration();
        if (fileConfiguration.getBoolean("Selector.Add_On_Join")) {
            int slotPosition = fileConfiguration.getInt("Selector.Slot.Position");
            ItemStack selector = LobbySwitch.p.getConfigManager().getSelector();
            if (slotPosition == -1) {
                if (!event.getPlayer().getInventory().contains(selector)) {
                    event.getPlayer().getInventory().addItem(new ItemStack[]{selector});
                }
            } else {
                event.getPlayer().getInventory().remove(selector);
                event.getPlayer().getInventory().setItem(slotPosition - 1, selector);
            }
        }

    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
            ItemStack itemStack = event.getItem();
            if (itemStack != null && itemStack.getType() == LobbySwitch.p.getConfigManager().getSelector().getType()
                    && itemStack.getItemMeta().getDisplayName().equals(LobbySwitch.p.getConfigManager().getSelector().getItemMeta().getDisplayName())) {
                event.getPlayer().openInventory(LobbySwitch.p.getConfigManager().getInventory());
            }
        }

    }

    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (channel.equals(LobbySwitch.p.getPluginChannel())) {
            try {
                var dataInput = new DataInputStream(new ByteArrayInputStream(message));
                String subChannel = dataInput.readUTF();
                if (subChannel.equals("GetServers")) {
                    for (var server : dataInput.readUTF().split(", ")) {
                        LobbySwitch.p.addServer(server);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
