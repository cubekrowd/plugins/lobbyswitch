package com.lobbyswitch.util;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

public class ItemUtil {
    // Doesn't matter which enchantment this is.
    private static final Enchantment GLOW_ENCHANT = Enchantment.LOYALTY;

    public static void addGlow(ItemStack item) {
        if (!item.hasItemMeta()) {
            return;
        }

        var itemMeta = item.getItemMeta();

        if (!itemMeta.hasEnchant(GLOW_ENCHANT)) {
            item.addUnsafeEnchantment(GLOW_ENCHANT, 0);
            item.setItemMeta(itemMeta);
        }
    }
}
