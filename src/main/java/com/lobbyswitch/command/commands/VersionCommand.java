package com.lobbyswitch.command.commands;

import com.lobbyswitch.LobbySwitch;
import com.lobbyswitch.command.ICommand;
import com.lobbyswitch.util.ChatUtils;
import java.util.List;
import org.bukkit.command.CommandSender;

public class VersionCommand implements ICommand {
    public String getName() {
        return "version";
    }

    public String[] getAliases() {
        return new String[]{"v"};
    }

    public String getPermission() {
        return "lobbyswitch.version";
    }

    public String getUsageString(String label, CommandSender sender) {
        return label;
    }

    public String getDescription() {
        return "Shows the version of Lobby Switch that you are using.";
    }

    public boolean canBeConsole() {
        return true;
    }

    public boolean canBeCommandBlock() {
        return true;
    }

    public boolean onCommand(CommandSender sender, String label, String... args) {
        if (args.length != 0) {
            return false;
        } else {
            sender.sendMessage(ChatUtils.t("version", LobbySwitch.p.getDescription().getVersion()));
            return true;
        }
    }

    public List<String> onTabComplete(CommandSender sender, String label, String... args) {
        return null;
    }
}
