package com.lobbyswitch;

public interface Callback<V> {
    void onSuccess(V var1);

    void onFailure();
}
